package com.enel.sustaining.utils;

import lombok.Getter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Map;

@Getter
public class XmlPattern {

    private Path templatePath;
    private Path requestPath;
    private String requestString;

    public XmlPattern(Map<String, String> data, String templatePathString) {
        this.templatePath = Paths.get(templatePathString);
        this.requestPath = Paths.get("src/req");
        Charset charset = StandardCharsets.UTF_8;

        try {
            Files.copy(templatePath, requestPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            String content = new String(Files.readAllBytes(requestPath), charset);

            for (Map.Entry<String, String> entry : data.entrySet()) {
                content = content.replaceAll(entry.getKey(), entry.getValue());
            }

            Files.write(requestPath, content.getBytes(charset));
            deleteRemainingLines(requestPath);

            this.requestString = new String(Files.readAllBytes(requestPath), charset);
            Files.deleteIfExists(requestPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public XmlPattern(Map<String, String> data, String templatePathString, String requestPathString) {
        this.templatePath = Paths.get(templatePathString);
        this.requestPath = Paths.get(requestPathString);
        Charset charset = StandardCharsets.UTF_8;

        try {
            Files.copy(templatePath, requestPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            String content = new String(Files.readAllBytes(requestPath), charset);

            for (Map.Entry<String, String> entry : data.entrySet()) {
                content = content.replaceAll(entry.getKey(), entry.getValue());
            }

            Files.write(requestPath, content.getBytes(charset));
            deleteRemainingLines(requestPath);

            this.requestString = new String(Files.readAllBytes(requestPath), charset);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteRemainingLines(Path file) {
        try {
            String regex = "(.*)<%=(.*)";
            Path tempFile = Paths.get("src/temp");
            Files.createFile(tempFile);
            Files.lines(file).forEach(
                    line -> {
                        if(!line.matches(regex))
                            try {
                                Files.write(tempFile, line.getBytes(), StandardOpenOption.APPEND);
//                                Files.write(tempFile, "\n".getBytes(), StandardOpenOption.APPEND);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                    }
            );
            Files.copy(tempFile, requestPath, StandardCopyOption.REPLACE_EXISTING);
            Files.deleteIfExists(tempFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
