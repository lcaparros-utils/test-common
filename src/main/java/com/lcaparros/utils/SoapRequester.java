package com.enel.sustaining.utils;

import lombok.Getter;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;

@Getter
public class SoapRequester {

    XmlPattern xmlPattern = null;
    String url;
    String soapRequestString;
    String soapResponse = null;
    String httpResponse = null;
    Map<String, String> headers = null;

    public SoapRequester(String soapServiceUrl, XmlPattern xmlPattern, Map<String, String> headers) {
        this.url = soapServiceUrl;
        this.xmlPattern = xmlPattern;
        this.soapRequestString = xmlPattern.getRequestString();
        this.headers = headers;
    }

    public SoapRequester(String soapServiceUrl, XmlPattern xmlPattern) {
        this.url = soapServiceUrl;
        this.xmlPattern = xmlPattern;
        this.soapRequestString = xmlPattern.getRequestString();
    }

    public int sendRequest() {

        int responseCode = 0;
        HttpResponse response;

        try {
            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type","text/xml");

            if (headers!=null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    post.setHeader(entry.getKey(), entry.getValue());
                }
            }

            post.setEntity(new StringEntity(soapRequestString));

            CloseableHttpClient client = HttpClientBuilder.create().build();

            response = client.execute(post);

            this.soapResponse = EntityUtils.toString(response.getEntity());

            responseCode = response.getStatusLine().getStatusCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseCode;
    }
}
