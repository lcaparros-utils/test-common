package com.enel.sustaining.utils;

import com.splunk.*;
import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Getter
public class SplunkManager {

    private Service service;
    private String username = "qaa";
    private String password = "qaa";
    private String host = "splunk.enernoc.cc";
    private int port = 8089;
    private String main_query;

    private Map<String, String> allMessages = new HashMap<>();

    public SplunkManager(String query) {
        main_query = "search " + query;
        login();
    }

    public SplunkManager(Map<String, String> splunkFilters) {
        main_query = "search index=" + splunkFilters.get("index") + " ";
        for (Map.Entry<String, String> entry : splunkFilters.entrySet()) {
            if(!entry.getKey().equals("index")) {
                main_query = main_query + "\"" + entry.getKey() + "=" + entry.getValue() + "\" ";
            }
        }
        login();
    }

    public SplunkManager(String index, String app, String lambda) {

        main_query = "search index="+index+" app="+app+" lambda="+lambda+" ERROR";
        login();
    }

    public void readAllSplunkMessages(String earliest_time, String latest_time) {

        String allLogs_query = main_query + " trace-id | rex \"\\[message=(?<message>[^\\]]*)\" | search message=* | table trace_id,message";

        JobArgs jobargs = new JobArgs();
        jobargs.put("earliest_time", earliest_time);
        jobargs.put("latest_time", latest_time);
        jobargs.setExecutionMode(JobArgs.ExecutionMode.BLOCKING);


        try {

            InputStream resultsNormalSearch =  service.oneshotSearch(allLogs_query, jobargs);

            ResultsReaderXml resultsReaderNormalSearch = new ResultsReaderXml(resultsNormalSearch);
            HashMap<String, String> event;
            while ((event = resultsReaderNormalSearch.getNextEvent()) != null) {
                allMessages.put(event.get("trace_id"), event.get("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean checkOneMessage(String trace_id, String expected) {

        String message_query = main_query + " trace_id=\""+trace_id+"\" | rex \"\\[message=(?<message>[^\\]]*)\" | search message=* | table message";
        InputStream results_oneshot =  service.oneshotSearch(message_query);

        try {
            ResultsReaderXml resultsReader = new ResultsReaderXml(results_oneshot);
            HashMap<String, String> event;


            while ((event = resultsReader.getNextEvent()) != null) {
                if(event.get("message").equals(expected)) {
                    System.out.println("event.get(\"message\") " + event.get("message"));
                    return true;
                }
            }
            resultsReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }



    public boolean checkMessageByTraceID(String traceID, String expected) {
        return allMessages.get(traceID).equals(expected);
    }

    public String logsToString() {
        String logs = "";
        try {
            InputStream results =  service.oneshotSearch(main_query);
            ResultsReaderXml resultsReader = new ResultsReaderXml(results);
            HashMap<String, String> event;
            while((event = resultsReader.getNextEvent()) != null) {
                for(HashMap.Entry<String, String> entry : event.entrySet()) {
                    logs = logs + entry.getKey() + "=" + entry.getValue() + " ";
                }
                logs = logs + "\n";
            }
            resultsReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logs;
    }

    public String logsToStringForTimeInterval(String earliest_time, String latest_time) {
        String logs = "";
        try {
            JobArgs jobargs = new JobArgs();
            jobargs.put("earliest_time", earliest_time);
            jobargs.put("latest_time", latest_time);
            jobargs.setExecutionMode(JobArgs.ExecutionMode.BLOCKING);
            InputStream results =  service.oneshotSearch(main_query, jobargs);
            ResultsReaderXml resultsReader = new ResultsReaderXml(results);
            HashMap<String, String> event;
            while((event = resultsReader.getNextEvent()) != null) {
                for(HashMap.Entry<String, String> entry : event.entrySet()) {
                    logs = logs + entry.getKey() + "=" + entry.getValue() + " ";
                }
                logs = logs + "\n";
            }
            resultsReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logs;
    }

    private void login() {
        HttpService.setSslSecurityProtocol( SSLSecurityProtocol.TLSv1_2 );
        ServiceArgs loginArgs = new ServiceArgs();
        loginArgs.setUsername(username);
        loginArgs.setPassword(password);
        loginArgs.setHost(host);
        loginArgs.setPort(port);
        service = Service.connect(loginArgs);
    }


}
